#ifndef _ASLOOKUP_H
#define _ASLOOKUP_H 1

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/* Limits */
#define ASLOOKUP_ASN_MIN 		1
#define ASLOOKUP_ASN_MAX 		64511
/* Private AS range 64512-65535 */
#define ASLOOKUP_TTL_OUT 		64

/* Query types */
#define ASLOOKUP_QUERY_TYPE_NONE 	0
#define ASLOOKUP_QUERY_TYPE_ORIGIN	1
#define ASLOOKUP_QUERY_TYPE_ORIGIN6	2
#define ASLOOKUP_QUERY_TYPE_ASN 	3
#define ASLOOKUP_QUERY_TYPE_PEER	4

#define ASLOOKUP_QUERY_SUCCESS 		0
#define ASLOOKUP_QUERY_FAILURE		-1

#define ASLOOKUP_INIT_SUCCESS		0
#define ASLOOKUP_INIT_FAILURE 		-1

/* Team Cymru zone information */
#define ASLOOKUP_ZONE_DOMAIN "asn.cymru.com"
#define ASLOOKUP_ZONE_ASN ASLOOKUP_ZONE_DOMAIN
#define ASLOOKUP_ZONE_ORIGIN "origin."ASLOOKUP_ZONE_ASN
#define ASLOOKUP_ZONE_ORIGIN6 "origin6."ASLOOKUP_ZONE_ASN
#define ASLOOKUP_ZONE_PEER "peer."ASLOOKUP_ZONE_ASN

#define ASLOOKUP_EXIT_SUCCESS 		0
#define ASLOOKUP_EXIT_FAILURE 		-1

/* Usual getopt stuff */
#define ASLOOKUP_GETOPT_NONE		0
#define ASLOOKUP_GETOPT_HELP 		1
#define ASLOOKUP_GETOPT_VERSION		2
#define ASLOOKUP_GETOPT_VERBOSE 	4
#define ASLOOKUP_GETOPT_4 		8
#define ASLOOKUP_GETOPT_6		16
#define ASLOOKUP_GETOPT_ASN		32
#define ASLOOKUP_GETOPT_PEER		64
/* Future features? */
#define ASLOOKUP_GETOPT_LOOKUP		128

#define ASLOOKUP_GETOPTS ":46apVvhz"
#define ASLOOKUP_ARGS "46apVvhz"

#define ASLOOKUP_VERSION "0.1"

/* Simple zone type to hostname matrix */
struct as_zone {
	int type;
	char *value;
} as_zones[] = {
	ASLOOKUP_QUERY_TYPE_ORIGIN, ASLOOKUP_ZONE_ORIGIN,
	ASLOOKUP_QUERY_TYPE_ORIGIN6, ASLOOKUP_ZONE_ORIGIN6,
	ASLOOKUP_QUERY_TYPE_ASN, ASLOOKUP_ZONE_ASN,
	ASLOOKUP_QUERY_TYPE_PEER, ASLOOKUP_ZONE_PEER,
	0, NULL
};

/* aslookup application */
typedef struct as_pack {
	/* Socket to send DNS packets */
	int s;

	/* Query information */
	int query_type;	

	/* Application options */
	unsigned char options;
} as_pack_t;

#endif /* _ASLOOKUP_H */
