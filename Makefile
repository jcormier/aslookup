#
# Simple aslookup Makefile
#
CC=gcc
CFLAGS=
# Uncomment the following line to include DEBUG code
# CFLAGS=-DEBUG
LDFLAGS=-lresolv
DEPS=aslookup.h
OBJ=aslookup.o

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

aslookup: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) $(LDFLAGS)

clean:
	rm -f *.o
	rm aslookup
