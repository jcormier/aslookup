/**
*
* aslookup version 0.1, Copyright (C) 2016 Jonathan Cormier <jonathan@cormier.co>
* License details can be found in LICENSE file.
*
* Command line tool to resolve BGP Autonmous System (AS) numbers
* and resolve IP address to their orign AS, and maybe some simple tracing.
*
* I have no idea if this tool will be useful or not, but here it is... 
*
* If one wants to find the origin AS of an Internet Protocol (IP) address, just do:
*
* $ aslookup 8.8.8.8
* 15169 | 8.8.8.0/24 | US | arin |
* $ aslookup AS15169
* 15169 | US | arin | 2000-03-30 | GOOGLE - Google Inc., US
*
* More information can be found @ https://cormier.co/aslookup
*
**/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <unistd.h>
#include <errno.h>

/* For is_asn function */
#include <limits.h>

#include <ctype.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* getaddrinfo(3) */
#include <sys/types.h>
#include <netdb.h>

/* res_ functions */
#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>

#include "aslookup.h"

void
strip(char *str) {
    char *p2 = str;

    while(*str != '\0') {
        if(*str != '\t' && *str != '\n') {
            *p2++ = *str++;
        } else {
            ++str;
        }
    }

    *p2 = '\0';
}

/* Query type to zone */
char *
q2z(int type) {
	struct as_zone *zones;
	
	/* Loop through zones, checking for type */
	for (zones = as_zones; zones->value != NULL; zones++) {
		/* Return zone for query type */
		if (zones->type == type) {
			return (zones->value);
		}
	}

	return (NULL);
}

/* Simle interface to getaddrinfo */
struct addrinfo *
resolv(const char *host, const char *serv, int family, int socktype) {
	int n;
	struct addrinfo hints, *res;
	
	memset(&hints, 0, sizeof(struct addrinfo));
	
	hints.ai_flags = AI_CANONNAME;
	hints.ai_family = family;
	hints.ai_socktype = socktype;

	n = getaddrinfo(host, serv, &hints, &res);
	
	/* Failed to resolve */
	if (n != 0) {
		return(NULL);
	}

	return(res);
}

void
pr_version(void) {
	printf("aslookup v%s\n", ASLOOKUP_VERSION);	
}

/* Output zone information to stdout */
void
pr_zones(void) {
	struct as_zone *zones;

	printf("\nZones:\n");

	/* Output zones */
	for (zones = as_zones; zones->value != NULL; zones++) {
		printf("%d: %s\n", zones->type, zones->value);
	}	
}

/* Reverse IP address string */
/* Free returned value */
char *
rev(char *str) {
	/* Reverse the input */
	char reversed_ip[INET_ADDRSTRLEN];
	in_addr_t addr;

        /* Get the textual address into binary format */
        inet_pton(AF_INET, str, &addr);

	/* Reverse the bytes in the binary address */
	addr = ((addr & 0xff000000) >> 24) |
        	((addr & 0x00ff0000) >>  8) |
		((addr & 0x0000ff00) <<  8) |
		((addr & 0x000000ff) << 24);

	/* And lastly get with textual representation */
	inet_ntop(AF_INET, &addr, reversed_ip, sizeof(reversed_ip));

	/* Dup string, reversed_ip is on the stack and we will need it. */
	return strdup(reversed_ip);
}

/* Output command line help */
void
usage(char *pname) {
	printf("aslookup v%s [Autonomous System Lookup Tool]\n\n", ASLOOKUP_VERSION);
	printf("Usage: %s -%s [ASN | IP Address]\n\n", pname, ASLOOKUP_ARGS);
	printf("  -4\tIPv4 to ASN (default)\n");
	printf("  -6\tIPv6 to ASN\n");
	printf("  -a\tASN Lookup\n");
	printf("  -p\tRetrieve Peers\n");
	printf("  -V\tVersion\n");
	printf("  -v\tVerbose\n");
	printf("  -h\tHelp\n\n");
	printf("Author: Jonathan Cormier <jonathan@cormier.co>\n");
}

/* ASN validation */
/* 0 - No, 1 - Yes */
int
is_asn(char *str) {
	int len = strlen(str);
	long number;
	char *end;

	if (len > 2) {
		/* Check for aAsS characters at the beginning of str */
		if (toupper(str[0]) != 'A' || toupper(str[1]) != 'S') {
			return FALSE;
		}
		
		/* Convert string to long */
		errno = 0;
		number = strtol(&str[2], &end, 10);
		
		/* Check if conversion failed */
		if ((errno = ERANGE &&
			(number == LONG_MAX || number == LONG_MIN)) ||
			(errno != 0 && number == 0)) {
			return FALSE;
		}

		/* No digits found */
		if (end == str) {
			return FALSE;
		}

		/* If the digits we found were between ASLOOKUP_ASN_MIN and ASLOOKUP_ASN_MAX */
		if (number >= ASLOOKUP_ASN_MIN &&
			number <= ASLOOKUP_ASN_MAX) {
#ifdef EBUG
			fprintf(stderr, "[debug] ASN: %ld\n", number);
#endif /* EBUG */
			return TRUE;
		}
	}
	
	return FALSE;
}

int
aslookup_init(as_pack_t *pack) {
	res_init();

#ifdef EBUG
	fprintf(stderr, "[debug] aslookup_init: done\n");
#endif /* EBUG */

	return ASLOOKUP_INIT_SUCCESS;
}

void
aslookup_done(as_pack_t *pack) {
#ifdef EBUG
	fprintf(stderr, "[debug] aslookup_done: done\n");
#endif /* EBUG */
}

char *
ai2a(struct addrinfo *ai) {
	struct sockaddr_in *sin;

	sin = (struct sockaddr_in *)ai->ai_addr;

	return (inet_ntoa((struct in_addr)sin->sin_addr));
}

void
print_rdata(const unsigned char *data) {
	u_int8_t numchars = data[0];
	const unsigned char *ptr = data + 1;
	int len = strlen(ptr);

#ifdef EBUG
	fprintf(stderr, "[debug] numchars: %d, len: %d\n", numchars, len);
#endif /* EBUG */

	/* String length check */
	if (numchars == len) {	
		printf("%.*s\n", numchars, ptr);
	}
}

int
parse_handle(ns_msg *handle) {
	int an_count = ns_msg_count(*handle, ns_s_an);
	ns_rr rr;
	int rr_ret, i;
	const unsigned char *rr_rdata;

	for (i = 0; i < an_count; i++) {
		rr_ret = ns_parserr(handle, ns_s_an, i, &rr);

		if (rr_ret < 0) {
			return -1;
		}

		rr_rdata = ns_rr_rdata(rr);

		/* Will there be multiple rdata structures? */
		print_rdata(rr_rdata);
	}
}

int
aslookup_query(as_pack_t *pack, char *query) {
	HEADER *hdr;
	unsigned char buf[65535], qhost[256], *zserver;
	char *q;
	struct sockaddr_in *ans, dest;
	ns_msg handle;
	ns_rr rr;
	int len;
	int type = pack->query_type;

	/* Convert query type to zone server */
	zserver = q2z(type);

#ifdef EBUG
	fprintf(stderr, "[debug] Query: %s\n", query);
#endif /* EBUG */

	if (pack->options & ASLOOKUP_GETOPT_VERBOSE) {	
		printf("Zone: %s\n", zserver);
	}
	
	q = query;

	if (type == ASLOOKUP_QUERY_TYPE_ORIGIN ||
		type == ASLOOKUP_QUERY_TYPE_ORIGIN6) {
		char *r = rev(q);
		q = r;
	} 

	memset(buf, 0, sizeof(buf));

	len = res_querydomain(q, zserver, C_IN, T_TXT, buf, sizeof(buf));
	hdr = (HEADER *)buf;

	/* Initialize message handle */
	if (ns_initparse(buf, len, &handle) < 0) {
		return ASLOOKUP_QUERY_FAILURE;
	}

	parse_handle(&handle);

	if (pack->options & ASLOOKUP_GETOPT_VERBOSE) {
		printf("Sent query\n");
	}

	if (type == ASLOOKUP_QUERY_TYPE_ORIGIN ||
		type == ASLOOKUP_QUERY_TYPE_ORIGIN6) {
		free(q);
	}
	
	return ASLOOKUP_QUERY_SUCCESS;
}

int
aslookup_simple_query(as_pack_t *pack, unsigned char *query) {
		/* Attempt to detect type of query, if one wasn't specificially set.*/
		if (pack->query_type == ASLOOKUP_QUERY_TYPE_NONE) {
			int bwd = isdigit(query[0]);
	
			/* Found number, assume IP address. */
			if (bwd) {
				/* How to determine IPv4 vs IPv6? Or just like the command line options handle it? */
				pack->query_type = ASLOOKUP_QUERY_TYPE_ORIGIN;
			} else {
				/* Test for valid AS number */
				if (is_asn(query) == FALSE) {
					fprintf(stderr, "'%s' invalid AS. Try again.\n", query);	
				}
	
#ifdef EBUG
				fprintf(stderr, "[debug] Valid ASN found in query '%s'\n", query);	
#endif /* EBUG */
	
				pack->query_type = ASLOOKUP_QUERY_TYPE_ASN;
			}
		}

		int query_ret = aslookup_query(pack, query);
		
		if (query_ret < 0) {
			fprintf(stderr, "%s: lookup failure: %d: %s\n",
				__PRETTY_FUNCTION__, query_ret, query);
			return query_ret;
		}
}

extern char *optarg;
extern int optopt;

/* Application entry point */
int
main(int argc, char *argv[]) {
	int opt, index;
	int bwd = FALSE;
	int is_term = TRUE;
	unsigned char *query, buf[256];
	char *token;

	/* aslookup app structure */
	as_pack_t pack;

	char *argv0 = argv[0];
	pack.options = ASLOOKUP_GETOPT_NONE;
	pack.query_type = ASLOOKUP_QUERY_TYPE_NONE;

	/* Parse options */
	while ((opt = getopt(argc, argv, ASLOOKUP_GETOPTS)) != -1) {
		switch(opt) {
			case 'p':
				pack.query_type = ASLOOKUP_QUERY_TYPE_PEER;
				break;
			case 'a':
				pack.query_type = ASLOOKUP_QUERY_TYPE_ASN;
				break;
			case '6':
				pack.query_type = ASLOOKUP_QUERY_TYPE_ORIGIN6;
				break;
			case '4':
				pack.query_type = ASLOOKUP_QUERY_TYPE_ORIGIN;
				break;
			case 'L':
				pack.options |= ASLOOKUP_GETOPT_LOOKUP;
				break;
			case 'v':
				pack.options |= ASLOOKUP_GETOPT_VERBOSE;
				break;
			case 'V':
				pr_version();
				exit(ASLOOKUP_EXIT_SUCCESS);
			case 'z':
				pr_zones();
				exit(ASLOOKUP_EXIT_SUCCESS);	
			case 'h': {
				usage(argv0);
				exit(ASLOOKUP_EXIT_SUCCESS);
			}
			default:
				break;
		}
	}

	/* Adjust argv/c after parsing arguments */	
	argv += optind;
	argc -= optind;

	/* Check for stdin tty */
	is_term = isatty(fileno(stdin));

#ifdef EBUG
	fprintf(stderr, "[debug] argc: %d, term: %d\n", argc, is_term);

	for (index = 0; index < argc; index++) {
		fprintf(stderr, "[debug] argv[%d] = [%s]\n", index, argv[index]);
	}
#endif /* EBUG */
	
	/* Verify arguments and term */
	if (argc < 1 && is_term) {
		usage(argv0);
		exit(ASLOOKUP_EXIT_SUCCESS);
	}
	
	if (aslookup_init(&pack) < 0) {
		fprintf(stderr, "aslookup_init: failed to initialize\n");
		exit(ASLOOKUP_EXIT_FAILURE);		
	}

	if (is_term == FALSE) {
		fread(buf, sizeof(buf), 1, stdin);	
	
		for (token = strtok(buf, " "); token; token = strtok(NULL, " ")) {
			/* Strip newlines */
			strip(token);
			query = token;

                	if (aslookup_simple_query(&pack, query) < 0) {
                        	fprintf(stderr, "aslookup_simple_query: lookup failure: %s\n", query);
                	}
		}
	}

	/* Loop through possible command line queries... */
	for (query = argv[0], index = 0; index < argc; index++) {
		/* Update query */
		query = argv[index];

		if (aslookup_simple_query(&pack, query) < 0) {
                        fprintf(stderr, "aslookup_simple_query: lookup failure: %s\n", query);
		}
	}

	aslookup_done(&pack);
	
	exit(ASLOOKUP_EXIT_SUCCESS);
}

/* EOF */
