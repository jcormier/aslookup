#!/bin/sh
#
# Location of aslookup binary
ASLOOKUP=./aslookup
#
# Output of aslookup peer query
ASLOOKUP_OUT=$($ASLOOKUP -p $1 | cut -d'|' -f1)
# Convert string to array
PEERS=($ASLOOKUP_OUT)

# Lookup the peer ASNs
for PEER in "${PEERS[@]}"
do
	$ASLOOKUP "AS$PEER"
done
